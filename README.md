# Dealer Inspire NodeJS Code Challenge

You, meet Dealer Inspire NodeJS code challenge.  Dealer Inspire NodeJS code challenge, meet... you.

## Getting Started

First of all, you'll need to fork this repository to somewhere else.  Make sure your code is publicly available in a git repository when you're done.   (Like Bitbucket or GitHub. If you're super-nerdy and host your own public repo, just make sure the server is up 24/7 - our code-review monkeys like to work in the middle of the night.)

You don't have to host a working copy of the code, we'll be checking it out locally to review it. That's a good reminder - we'll need you to update this document with instructions on how to run your project locally - and what the end-point (and any optional headers, etc) are required to use the API.

## The Challenge

For this code challenge, you'll be making an API end-point that interacts with the [NHTSA Vehicle API](https://vpic.nhtsa.dot.gov/api/).  The end point should accept a query parameter of a VIN.  Response information will be decoded using the API provided by the NHTSA.  

Certain customers will need certain information in your response manipulated:

**Customer #4:** Any Models that begin with a number, need to have the word "Model" in front of it - for some reason, beginning this field with a number causes their software to break otherwise.

**Customer #99:** Requires every make and Model to be all caps.  All other fields should retain their capitalization issued from the NHTSA.

**Customer #12:** Is not authorized to work with anyone but Honda and Acura.  Any VINs that result in a make outside of their authorization should be rejected.

## Expectations

You deal with REST and JSON only (no XML, SOAP, etc).

You create your own data model for a response.  Don't just proxy the NHTSA information back to us.  (Hint: if you find yourself working with the ValueId field, you probably are doing it wrong.)

You've specified some method to indicate which customer is requesting this information - and that information is required.  It could be a header, or a query string or any other way of doing it.  Anonymous requests are not authorized (you don't have to write an authentication system - we'll "trust" whatever method you identify the customer with).

You provide unit and/or end-to-end tests for your code.
